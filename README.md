# Cube Run

## Description

Simples jogo em Unity seguindo um tutorial.

## Links
- [Play](https://master.d3qmg4fk063k5p.amplifyapp.com/src/Games/CubeRun/index.html);
- [YouTube - Brackeys - Unity Beginner Tutorials (Tutorial)](https://www.youtube.com/playlist?list=PLPV2KyIb3jR5QFsefuO2RlAgWEz6EvVi6);
