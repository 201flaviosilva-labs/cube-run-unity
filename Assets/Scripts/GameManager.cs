﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public bool isGameStoped = false;
    public float delayToRestart;

    public GameObject completeLevelUIPanel;

    public void GameOver()
    {
        isGameStoped = true;
        Invoke("RestartGame", delayToRestart);
    }

    private void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void EndLevel()
    {
        completeLevelUIPanel.SetActive(true);
    }

    public void ChangeSceneTo_Home()
    {
        SceneManager.LoadScene("Home");
    }

    public void ChangeSceneTo_Level1()
    {
        SceneManager.LoadScene("Level1");
    }

    public void ChangeSceneTo_NextLevel()
    {
        SceneManager.LoadScene("Level1");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
