﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rb;
    public float speedX;
    public float speedZ;

    void FixedUpdate()
    {
        var h = Input.GetAxis("Horizontal");
        Vector3 newVelocity = new Vector3(h, 0, speedZ);
        rb.MovePosition(transform.position + newVelocity * speedX * Time.deltaTime);
        //rb.AddForce(newVelocity * 2000 * Time.deltaTime);
    }
}
