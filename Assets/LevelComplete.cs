﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelComplete : MonoBehaviour
{
    public void NextLevel()
    {
        int activeSceneBuildIndex = SceneManager.GetActiveScene().buildIndex;
        if (SceneManager.GetAllScenes().Length < activeSceneBuildIndex) SceneManager.LoadScene("Home");
        else SceneManager.LoadScene(activeSceneBuildIndex + 1);
    }
}
